import struct

from ...utils.binary import _get_str, _get_int



# https://en.uesp.net/wiki/Skyrim_Mod:Fields



FIELD_TYPES = {
    'HEDR': ('float32', 'uint32', 'uint32'),
    'CNAM': 'zstring',
    'SNAM': 'zstring',
    'MAST': 'zstring',
    'EDID': 'zstring',
}



class Field:
    """Fields handle fine details of a Record. Fields could be used across various types of records."""
    header_size = 6

    def __init__(self, content: bytes):
        if len(content) < self.header_size:
            raise ValueError(f'Field content is too small: {len(content)}')
        self.name = _get_str(content[0:4])
        self.size = _get_int(content[4:6])
        self.bytes = content[self.header_size:self.header_size + self.size]

    def __getitem__(self, item):
        if isinstance(item, int):
            return self.bytes[item]

        elif isinstance(item, slice):
            return self.bytes[item]

        raise TypeError(f'Item must be an integer or slice, not {type(item)}')

    def __repr__(self):
        return f'{self.name}: {self.bytes}'

    def __hex__(self):
        return hex(_get_int(self.bytes))

    def __str__(self):
        return _get_str(self.bytes)

    def __int__(self):
        return _get_int(self.bytes)

    def __float__(self, offset: int = 0):
        return struct.unpack('f', self.bytes)[0 + offset]

    def __len__(self):
        return self.header_size + self.size

    def __call__(self):
        if self.name in FIELD_TYPES:
            field_type = FIELD_TYPES[self.name]

            if isinstance(field_type, tuple):
                value = []
                for _type in field_type:
                    _pos = field_type.index(_type) * 4
                    if _type == 'float32':
                        value += [self.__float__(_pos)]
                    elif _type == 'uint32':
                        value += [_get_int(self.bytes[_pos:_pos + 4])]
                return tuple(value)

            elif field_type == 'zstring':
                return _get_str(self.bytes)

            raise NotImplementedError(f'Field type {self.name} is not implemented yet')
            
        return self.bytes
