from typing import Iterable
import mmap

from .core import Record
from ...utils.binary import _get_bit, _get_int, _get_str



#region Plugin Info / Header
class TES4(Record):

    @property
    def author(self):
        return str(self['CNAM'])

    @property
    def masters(self):
        try:
            return self._masters
        except AttributeError:
            self._masters = [str(master) for master in self.get_fields('MAST')]
            return self._masters

    @property
    def is_esm(self):
        return self._get_flag(0)

    @property
    def is_esl(self):
        return self._get_flag(9)
#endregion



#region Form Group
class Group(Record):
    def __post_init__(self):
        super().__post_init__()
        if self._header[0:4].decode('ascii') != 'GRUP':
            raise TypeError(f'Group record must have the type GRUP.')

    @property
    def label(self):
        if self.type == 0:
            return self._header[8:12].decode('ascii')
        elif self.type in [1, 6, 7, 8, 9]:
            raise NotImplementedError('Group label is only available for top-level groups.')
            # TODO: Return a FormID
        else:
            raise NotImplementedError('Group label is only available for top-level groups.')

    @property
    def type(self):
        return _get_int(self._header[12:16])

    @property
    def version(self):
        return int.from_bytes(self._header[18:20], 'little', signed=False)

    @property
    def is_top_level(self):
        return self.type == 0

    def _get_all_records(self, starting_pointer: int=0) -> Iterable[Record]:
        pointer = self._pointer + self.header_size + starting_pointer
        while pointer < self._pointer + self.size:
            if self._mmap[pointer:pointer + 4].decode('ascii') == 'GRUP':
                group = Group(self._mmap, pointer)
                if group.is_top_level:
                    yield from group._get_all_records()
                pointer += group.size
            else:
                record = Record(self._mmap, pointer)
                yield record
                pointer += record.size + Record.header_size

#endregion



#region Record Types
class NPC_(Record):                                                                                                             # TODO Errors ? No _get_bit() found, no self.acbs... Remnants from old version?

    @property
    def is_female(self):
        return self._get_bit(self.acbs, 0)

    @property
    def is_essential(self):
        return self._get_bit(self.acbs, 1)

    @property
    def is_preset(self):
        return self._get_bit(self.acbs, 2)

    @property
    def respawns(self):
        return self._get_bit(self.acbs, 3)

    @property
    def auto_calculate_stats(self):
        return self._get_bit(self.acbs, 4)

    @property
    def is_unique(self):
        return self._get_bit(self.acbs, 5)

    @property
    def is_levelling_up_with_pc(self):
        return self._get_bit(self.acbs, 7)

    @property
    def is_protected(self):
        return self._get_bit(self.acbs, 11)

    @property
    def is_summonable(self):
        return self._get_bit(self.acbs, 14)

    @property
    def has_opposite_gender_animations(self):
        return self._get_bit(self.acbs, 19)

    @property
    def is_ghost(self):
        return self._get_bit(self.acbs, 29)

    @property
    def is_invulnerable(self):
        return self._get_bit(self.acbs, 31)

    @property
    def level(self):
        if self.is_levelling_up_with_pc:
            divider = 1000
        else:
            divider = 1
        return int.from_bytes(self.acbs[8:10], 'little', signed=False) / divider

    @property
    def face_geom_file_name(self) -> str:
        return f'00{str(self.form_id.objectindex)[2:].rjust(8, "0")}.nif'

    def get_face_geom_path_name(self, mod_file_name: str) -> str:
        """Return the path under data for the FaceGenData mesh."""
        return '\\'.join(['Meshes',
                          'Actors',
                          'Character',
                          'FaceGenData',
                          'FaceGeom',
                          mod_file_name,
                          self.face_geom_file_name])

    @property
    def face_tint_file_name(self) -> str:
        return f'00{str(self.form_id.objectindex)[2:].rjust(8, "0")}.dds'

    def get_face_tint_path_name(self, mod_file_name: str) -> str:
        """Return the path under data for the FaceGenData mesh."""
        return '\\'.join(['Textures',
                          'Actors',
                          'Character',
                          'FaceGenData',
                          'FaceTint',
                          mod_file_name,
                          self.face_tint_file_name])



class BOOK(Record):
    pass



# TODO Other classes = for each Record Type
#endregion
