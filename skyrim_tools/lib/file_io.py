from dataclasses import dataclass
from pathlib import Path
from io import BufferedReader
import mmap



@dataclass
class Loader:
    """Base class for reading binary files."""
    file_path: Path
    file_name: "str|None" = None
    _file: "BufferedReader|None" = None
    _mmap: "mmap.mmap|None" = None

    def __post_init__(self):
        if not self.file_path.exists():
            raise FileNotFoundError
        self.file_name = self.file_path.name
        self._file = open(self.file_path, 'rb')
        self._mmap = mmap.mmap(self._file.fileno(), length=0, access=mmap.ACCESS_READ)

    def _read_bytes(self, pos: int, length: int = 1) -> bytes:
        self._mmap.seek(pos)
        return self._mmap.read(length)

    def _read_string(self, _pos: int, encoding: str = 'utf-8') -> str:
        _bytes = self._read_bytes(_pos)
        while _bytes[-1] != 0:
            _pos += 1
            _bytes += self._read_bytes(_pos)

        return _bytes[:-1].decode(encoding)

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_val, trace):
        self._file.close()
