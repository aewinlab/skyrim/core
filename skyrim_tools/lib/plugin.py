from dataclasses import dataclass, field
from enum import Enum
from pathlib import Path
from typing import List, Iterator

from .models import ModType, RecordRef
from .file_io import Loader
from ..utils.binary import _get_int
from ..utils.debug import _debug_records
from .record.core import Record
from .record.types import Group, TES4






@dataclass
class ElderScrollsFile(Loader):
    """Parse a ESM/P/L file.

    Usage example:

    from skyrim_tools import ElderScrollsFile
    mod_file = Path("path/to/your/mod/mod_name.esp")
    with ElderScrollsFile(mod_file) as mod:
        # Do something
    """

    header_record: "TES4|None" = None
    is_esm: "bool|None" = None
    is_esl: "bool|None" = None
    masters: list[str] = field(default_factory=list)
    author: "str|None" = None
    record_count: int = 0
    _record_positions: dict = field(default_factory=dict)
    _pos: dict = field(default_factory=dict)

    def __post_init__(self):
        super().__post_init__()
        try:
            assert self._read_bytes(0, 4) == b'TES4'
        except AssertionError:
            raise RuntimeError('Incorrect file header - is this a TES4 file?')
        self.header_record = TES4(self._mmap, 0)
        self.is_esm = self.header_record.is_esm
        self.is_esl = self.header_record.is_esl
        self.masters = self.header_record.masters
        self.author = self.header_record.author
        self.record_count = _get_int(self.header_record['HEDR'][4:8])

    def __getitem__(self, key):
        if isinstance(key, slice):
            if key.step is not None:
                raise KeyError(f'{self.__class__.__name__} does not allow slicing '
                                'with a step. Use only one colon in slice, for example: [0:4]')
            return self._read_bytes(key.start, key.stop - key.start)
        elif isinstance(key, int):
            raise NotImplementedError
        elif isinstance(key, Record):
            raise NotImplementedError
        elif isinstance(key, str):
            if len(key) == 4:
                return [record for record in self.records.values() if record.type == key]
            elif key[:2] == '0x':
                raise NotImplementedError
        else:
            raise KeyError

    def _get_type_at_position(self, pos: int) -> str:
        return self._mmap[pos:pos + 4].decode('ascii')

    def _get_record_at_position(self, pos: int) -> Record:
        return Record(self._mmap, pos)


    def _get_records_by_type(self, record_type: str, starting_position: int = 0) -> Record:
        _pos = starting_position
        while _pos < len(self._mmap):
            if self._get_type_at_position(_pos) == 'GRUP':
                group = Group(self._mmap, _pos)
                print("=== GRUP ===", group.type, group.label)
                if group.is_top_level and group.label == record_type:
                    for record in group._get_all_records():
                        print(record.type)
                        yield record
                        _pos += record.size + record.header_size
                else:
                    _pos += group.size

            else:
                record = Record(self._mmap, _pos)
                print(record.type)
                if record.type == record_type:
                    yield record
                _pos += record.size + record.header_size


    def _get_all_records(self, starting_position: int = 0) -> Record:
        _pos = starting_position
        while _pos < len(self._mmap):
            if self._get_type_at_position(_pos) == 'GRUP':
                group = Group(self._mmap, _pos)
                if group.is_top_level:
                    for record in group._get_all_records():
                        yield record
                _pos += group.size
            else:
                record = Record(self._mmap, _pos)
                yield record
                _pos += record.size + record.header_size


from ..constants import SPID_TYPES
@dataclass
class Mod:
    path: Path
    name: "str|None" = None
    author: "str|None" = None
    type: list[ModType] = field(default_factory=list)
    # masters: List['Mod'] = field(default_factory=list)
    masters: list[str] = field(default_factory=list)
    records: dict[RecordRef] = field(default_factory=dict)                  # ALT: None = None

    _options: "dict|None" = None

    def __post_init__(self):
        self.name = self.path.name
        self.parse_data()

    def parse_data(self):
        with ElderScrollsFile(file_path=self.path) as plugin:
            self.author = plugin.author
            self.masters = plugin.masters

            if plugin.is_esm: self.type += [ModType['ESM']]
            if plugin.is_esl: self.type += [ModType['ESL']]
            if not self.type: self.type += [ModType['ESP']]

            framework_model = self._options.get('framework_model', {}) if self._options else {}
            selected_record_types = self._options.get('selected_types') if self._options else None
            trim_records_data = self._options.get('trim_records') if self._options else False
            sort_records_alphabetically = self._options.get('sort_records') if self._options else False

            self.get_records(plugin=plugin, model=framework_model, select=selected_record_types, trim=trim_records_data, alpha_order=sort_records_alphabetically)
            # NOTE Example: self.get_records(plugin=plugin, model=SPID_TYPES, select=('OTFT', 'SPEL', 'PERK'))
            # self.get_records(plugin=plugin, select='NPC_')

            # TODO Add plugin.record_count?

    def get_records(self, plugin: ElderScrollsFile, model: dict = {}, select: "str|tuple|None" = None, trim: bool = False, alpha_order: bool = False):
        records = [r for r in plugin._get_all_records()]

        # # DEBUG
        # _debug_records(records=records)

        if select and isinstance(select, str):
            select = (select,)
        
        for record in records:

            # Check if the record type is of interest or not
            if select and not record.type in select: continue

            # Get the key matching for the record type if a model was given (eg. SPID_TYPES...)
            record_category = [k for k,v in model.items() if v and (record.type in v or record.type == v)]
            if model and len(record_category) != 1: continue

            record_type = record_category[0] if model else record.type

            # Build the RecordRef depending on record type
            try:
                if record.type == 'NPC_':
                    npc_data = record.content

                    # Get EditorID: find corresponding header in byte content, then format and decode it
                    i = npc_data.find(b'EDID')
                    editor_id = None if i == -1 else b''.join(npc_data[i:].split(b'\x00')[1:2]).decode('utf-8')

                    # Get Name: find corresponding header in byte content, then format and decode it
                    i = npc_data.find(b'FULL')
                    name = None if i == -1 else b''.join(npc_data[i:].split(b'\x00')[1:2]).decode('utf-8')

                else:
                    editor_id = record.editor_id
                    name = record.full_name if record.full_name != "None" else None

                reference = {
                    record_type: [RecordRef(
                        form_id=str(record.form_id.objectindex),
                        editor_id=editor_id,
                        name=name,
                        mod=None if trim else self.name,
                        type=record.type)]
                    }
            
            except Exception as error:
                continue
            
            # Add the new reference to Mod records
            self.records = {
                k: self.records.get(k, []) + reference.get(k, [])
                for k in set(self.records | reference)
                }
            
        # Sort alphabetically if needed
        if alpha_order:
            self.records = {
                record_type: sorted(references, key=lambda x: x.editor_id.lower())
                for record_type, references in self.records.items()
                }

            # # DEBUG
            # print(reference)
