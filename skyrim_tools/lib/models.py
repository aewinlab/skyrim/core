from dataclasses import dataclass
from enum import Enum


# https://en.uesp.net/wiki/Skyrim_Mod:Mod_File_Format#Records
class ModType(Enum):
    ESP = hex(0)        # 0
    ESM = hex(1)        # 1
    ESL = hex(512)      # 200
    # ESPFE = 11        --> also as ESL


# https://en.uesp.net/wiki/Skyrim_Mod:Mod_File_Format#Groups
class RecordType(Enum):
    # # # # # GMST                          # Game Setting
    KYWD = 'keyword'                        # Keyword                                       # SPID, KID
    # # # # # LCRT                          # Location Reference Type
    # # # # # AACT                          # Action
    # # # # # TXST                          # Texture Set
    # # # # # GLOB                          # Global Variable
    CLAS = 'class'                          # Class
    FACT = 'faction'                        # Faction                                       # SPID
    # # # # # HDPT                          # Head Part
    # # # # # HAIR                          # Hair, Removed?
    # # # # # EYES                          # Eyes
    RACE = 'race'                           # Race / Creature Type
    # # # # # SOUN                          # Sound
    # # # # # ASPC                          # Acoustic Space
    # # # # # MGEF                          # Magic Effect
    # # # # # SCPT                          # ?????
    # # # # # LTEX                          # Land Texture
    # # # # # ENCH                          # Enchantment
    SPEL = 'spell'                          # Spell                                         # SPID
    SCRL = 'scroll'                         # Scroll                                        # SPID
    ACTI = 'activator'                      # Activator
    # # # # # TACT                          # Talking Activator
    ARMO = 'armor'                          # Armor                                         # SPID
    BOOK = 'book'                           # Book                                          # SPID
    CONT = 'container'                      # Container                                     # CID
    # DOOR                                  # Door
    INGR = 'ingredient'                     # Ingredient                                    # SPID
    # LIGH                                  # Light
    MISC = 'misc'                           # Misc. Object                                  # SPID
    # # # # # APPA                          # Apparatus (probably unused)
    STAT = 'static'                         # Static
    # # # # # SCOL                          # ?????
    # MSTT                                  # Movable Static
    # # # # # PWAT                          # ?????, Unsupported?
    # # # # # GRAS                          # Grass
    # TREE                                  # Tree
    # # # # # CLDC                          # Related to Hair?, Removed?
    FLOR = 'flora'                          # Flora
    FURN = 'furniture'                      # Furniture
    WEAP = 'weapon'                         # Weapon                                        # SPID
    AMMO = 'ammo'                           # Ammo                                          # SPID
    NPC_ = 'npc'                            # Actor (NPC, Creature)                         # SPID
    LVLN = 'lvl_npc'                        # Leveled Actor                                 # SPID
    KEYM = 'key'                            # Key                                           # SPID
    ALCH = 'potion'                         # Potion                                        # SPID
    # IDLM                                  # Idle Marker
    # COBJ                                  # Constructible Object (recipes)
    # # # # # PROJ                          # Projectile
    # # # # # HAZD                          # Hazard
    SLGM = 'soul_gem'                       # Soul Gem                                      # SPID
    LVLI = 'lvl_item'                       # Leveled Item                                  # SPID
    # WTHR                                  # Weather
    # # # # # CLMT                          # Climate
    # # # # # SPGD                          # Shader Particle Geometry
    # # # # # RFCT                          # Visual Effect
    # # # # # REGN                          # Region (Audio/Weather)
    # # # # # NAVI                          # Navigation (master data)
    # # # # # CELL                          # Celle
    # # # # # WRLD                          # Worldspace
    # # # # # DIAL                          # Dialog Topic
    # # # # # QUST                          # Quest
    # IDLE                                  # Idle Animation
    PACK = 'package_ai'                     # AI Package                                    # SPID
    # # # # # CSTY                          # Combat Style
    # # # # # LSCR                          # Load Screen
    LVSP = 'lvl_spell'                      # Leveled Spell                                 # SPID
    ANIO = 'anim_object'                    # Animation Object
    # # # # # WATR                          # Water Type
    # # # # # EFSH                          # Effect Shader
    # # # # # EXPL                          # Explosion
    # # # # # DEBR                          # Debris
    # # # # # IMGS                          # Image Space
    # # # # # IMAD                          # Image Space Modifier
    FLST = 'form_list'                      # Form List (non-leveled list)
    PERK = 'perk'                           # Perk                                          # SPID
    # # # # # BPTD                          # Body Part Data
    # # # # # ADDN                          # Addon Note
    # # # # # AVIF                          # Actor Values/Perk Tree Graphics
    # # # # # CAMS                          # Camera Shot
    # # # # # CPTH                          # Camera Path
    # # # # # VTYP                          # Voice Type
    # # # # # MATT                          # Material Type
    # # # # # IPCT                          # Impact Data
    # # # # # IPDS                          # Impact Data Set
    # # # # # ARMA                          # Armor Addon (Model)
    # # # # # ECZN                          # Encounter Zone
    # # # # # LCTN                          # Location
    # # # # # MESG                          # Message
    # # # # # RGDL                          # ?????
    # # # # # DOBJ                          # Default Object Manager
    # # # # # LGTM                          # Lighting Template
    # # # # # MUSC                          # Music Type
    # # # # # FSTP                          # Footstep
    # # # # # FSTS                          # Footstep Set
    # # # # # SMBN                          # Story Manager Branch Node
    # # # # # SMQN                          # Story Manager Quest Node
    # # # # # SMEN                          # Story Manager Event Node
    # # # # # DLBR                          # Dialog Branch
    # # # # # MUST                          # Music Track
    # # # # # DLVW                          # Dialog View
    # WOOP                                  # Word Of Power
    SHOU = 'shout'                          # Shout                                         # SPID
    # # # # # EQUP                          # Equip Slot (flag-type values)
    # # # # # RELA                          # Relationship
    # # # # # SCEN                          # Scene
    # # # # # ASTP                          # Association Type
    OTFT = 'outfit'                         # Outfit                                        # SPID
    # ARTO                                  # Art Object
    # # # # # MATO                          # Material Object
    # # # # # MOVT                          # Movement Type
    # # # # # HAZD                          # Hazard, Duplicate?
    # # # # # SNDR                          # Sound Reference
    # # # # # DUAL                          # Dual Cast Data (possibly unused)
    # # # # # SNCT                          # Sound Category
    # # # # # SOPM                          # Sound Output Model
    # # # # # COLL                          # Collision Layer
    # # # # # CLFM                          # Color
    # # # # # REVB                          # Reverb Parameters



@dataclass
class RecordRef:
    form_id: str
    editor_id: str
    name: "str|None"
    type: str
    mod: "str|None" = None
