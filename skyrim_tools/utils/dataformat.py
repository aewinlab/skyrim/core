import logging



def clean_strings(input: list[str], chars: str = " ") -> list[str]:
    return [item.strip(chars) for item in input if item.strip(chars) != ""]


def strip_words(s: str, words: tuple[str, ...]) -> str:

    while s.endswith(words) or s.startswith(words):
        for word in words:
            if s.startswith(word): s = s.removeprefix(word)
            elif s.endswith(word): s = s.removesuffix(word)

    return s


def str_to_tuple(string: str) -> tuple:
    data = [_.strip(" ") for _ in string.strip("()").split(',')]

    if len(data) < 1: return ()

    formatted_data = []

    for item in data:
        if item.isdigit():
            formatted_data.append(int(item))
        elif item.isdecimal():
            formatted_data.append(float(item))
        elif item.startswith("'") and item.endswith("'"):
            formatted_data.append(item.strip("'"))
        elif item.startswith('"') and item.endswith('"'):
            formatted_data.append(item.strip('"'))
        else:
            raise TypeError(f"Invalid type: {item}")
    
    return tuple(formatted_data)


def str_to_bool(string: str) -> bool:
    if string.lower() == "true":
        return True
    elif string.lower() == "false":
        return False
    else:
        raise ValueError(f'Invalid value: {string} is not a boolean')






#region DEPRECATED
def form_to_tuple(string: str):
    logging.warning("Function form_to_tuple() is deprecated. Use str_to_tuple() instead.")
#endregion
