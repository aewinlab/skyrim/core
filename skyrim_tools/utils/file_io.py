from pathlib import Path
import logging
import json



def load_data(file: Path) -> "dict | str | None":
    is_json = True if file.suffix == ".json" else False
    is_txt = True if file.suffix in (".txt", ".ini") else False

    if not any(i for i in (is_json, is_txt)): return

    with open(file, 'r', encoding='utf-8') as f:
        return json.load(f) if is_json else f.read()


def save_data(data: "dict | str", file: Path) -> None:
    if not isinstance(data, (dict, str)):
        raise TypeError("Invalid type: data should be either a string or a dictionnary")

    is_json = True if file.suffix == ".json" else False
    is_txt = True if file.suffix in (".txt", ".ini") else False

    if not (is_json or is_txt):
        raise TypeError('Invalid type: file extension should be either "txt", "ini" or "json"')

    with file.open('w', encoding='utf-8') as f:
        if is_json and type(data) == dict: json.dump(data, f, ensure_ascii=False, indent=4)
        elif is_txt and type(data) == str: f.write(data)


def find_matching_files(folder: Path, pattern: str = "", pos: str = "", suffix: str = "", child_only: bool = False) -> list[Path]:
    """Find all files with a given pattern in their name into a folder and its subfolders.
    Optional criteria can be passed to search only one side of file names, or to filter files on their extension.
    Returns a list of paths to the matching files.

    Args:
        folder: Path of the folder in which to search.
        pattern: Pattern to search in the file names.
                 Possible values: any string or "" (no pattern) by default.
        pos: Position where to search the pattern in the file names.
             Possible values: "l" (left), "r" (right) or "" (both sides) by default.
        suffix: File suffix (extension) the files should match.
                Possible values: any string (with "." or not) or "" (any suffix) by default.
        child_only: Search only into child folders if set to True.

    Example:
        To find all '.txt' files that end with '_TEXT' in the 'data' folder:
        >>> folder = Path("data")
        >>> matching_files = find_matching_files(folder, pattern="_TEXT", suffix="txt", pos="r")
        >>> print(matching_files)
    """

    suffix = f"{'' if suffix.startswith('.') else '.'}{suffix.lower()}" if suffix else ""
    pos = pos.lower()

    if pos.lower() not in ("", "r", "l"):
        raise ValueError(f'Invalid argument: "{pos}" is not a valid position')
    
    if child_only:
        # files = [child.glob(f"*{pattern}{'*' if pattern else ''}") for child in folder.iterdir() if child.is_dir()]
        files = []
        for child in folder.iterdir():
            if not child.is_dir(): continue
            files += list(child.glob(f"*{pattern}{'*' if pattern else ''}"))
    else:
        files = folder.rglob(f"*{pattern}{'*' if pattern else ''}")
    
    if suffix: files = filter(lambda f: f.suffix.lower() == suffix, files)

    if pos == "l": files = filter(lambda f: f.stem.startswith(pattern), files)
    elif pos == "r": files = filter(lambda f: f.stem.endswith(pattern), files)
    
    return list(files)




#region DEPRECATED
def save_text(text: str, file_path: Path):
    logging.warning("Function save_text() is deprecated. Use save_data() instead.")

def load_json(file_path: Path):
    logging.warning("Function load_json() is deprecated. Use load_data() instead.")

def get_lines(f: Path):
    logging.warning("Function get_lines() is deprecated. Use load_data(text_file).splilines() instead.")

def strip_words(s: str, words: tuple[str, ...]):
    logging.warning("Function strip_words() is deprecated. Use dataformat.strip_words() instead.")

def clean_lines(lines: list[str]):
    logging.warning("Function clean_lines() is deprecated. Use dataformat.clean_strings() instead.")
#endregion
