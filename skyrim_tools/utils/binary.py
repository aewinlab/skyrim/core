
STRING_ENCODINGS = ['utf-8', 'windows-1252']



def _get_bit(longword: bytes, bit: int) -> "bool|None":
    """Check the value of the bit at the given position (0 for the rightmost bit) of a longword (byte sequence).
    Returns True or False based on if the bit is set (1) or not (0)."""

    try:
        return bool(int.from_bytes(longword, 'little', signed=False) & 2 ** bit)
    except TypeError:
        if longword is None:
            return None


def _get_int(content: bytes) -> int:
    """Converts a byte sequence into an unsigned integer."""

    return int.from_bytes(content, 'little', signed=False)


def _get_str(content: bytes, encoding='utf-8') -> "str|None":
    """Attempts to decode a byte sequence into a string using different encodings (default is 'utf-8')."""

    for encoding in STRING_ENCODINGS:
        try:
            return content.decode(encoding).strip('\0')
        except UnicodeDecodeError:
            pass