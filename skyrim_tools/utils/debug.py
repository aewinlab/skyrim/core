

def _debug_records(records):
    for record in records:
        # # DEBUG
        # print(record._header)
        # print(record.content)
        # print(vars(record))
        # print([i for i in record.get_all_fields()])

        # Spell
        if record.type == 'SPEL':
            print("TYPE: ", record.type)
            try: print(f"FORMID: |{record.form_id.objectindex}|")
            except: input("FORMID: ERROR")
            try: print(f"EDID: |{record.editor_id}|")
            except: input("EDID: ERROR")
            try: print(f"NAME: |{record.full_name}|")
            except: input("NAME: ERROR")
            print("")

        
        # Perk
        if record.type == 'PERK':
            print("TYPE: ", record.type)
            try: print(f"FORMID: |{record.form_id.objectindex}|")
            except: input("FORMID: ERROR")
            try: print(f"EDID: |{record.editor_id}|")
            except: input("EDID: ERROR")
            try: print(f"NAME: |{record.full_name}|")
            except: input("NAME: ERROR")
            print("")


        # Item
        if record.type in ['SCRL', 'ARMO', 'BOOK', 'INGR', 'MISC', 'WEAP', 'AMMO', 'KEYM', 'ALCH', 'SLGM', 'LVLI']:
            print("TYPE: ", record.type)
            try: print(f"FORMID: |{record.form_id.objectindex}|")
            except: input("FORMID: ERROR")
            try: print(f"EDID: |{record.editor_id}|")
            except: input("EDID: ERROR")
            try: print(f"NAME: |{record.full_name}|")
            except: input("NAME: ERROR")
            print("")

        
        # Shout
        if record.type == 'SHOU':
            print("TYPE: ", record.type)
            try: print(f"FORMID: |{record.form_id.objectindex}|")
            except: input("FORMID: ERROR")
            try: print(f"EDID: |{record.editor_id}|")
            except: input("EDID: ERROR")
            try: print(f"NAME: |{record.full_name}|")
            except: input("NAME: ERROR")
            print("")
        

        # LevSpell (Leveled Spell)
        if record.type == 'LVSP':
            print("TYPE: ", record.type)
            try: print(f"FORMID: |{record.form_id.objectindex}|")
            except: input("FORMID: ERROR")
            try: print(f"EDID: |{record.editor_id}|")
            except: input("EDID: ERROR")
            try: print(f"NAME: |{record.full_name}|")
            except: input("NAME: ERROR")
            print("")
        

        # Package - Useless wihtout the number value for a NPC?
        if record.type == 'PACK':
            print("TYPE: ", record.type)
            try: print(f"FORMID: |{record.form_id.objectindex}|")
            except: input("FORMID: ERROR")
            try: print(f"EDID: |{record.editor_id}|")
            except: input("EDID: ERROR")
            try: print(f"NAME: |{record.full_name}|")
            except: input("NAME: ERROR")
            print("")
        

        # Outfit
        if record.type == 'OTFT':
            print("TYPE: ", record.type)
            try: print(f"FORMID: |{record.form_id.objectindex}|")
            except: input("FORMID: ERROR")
            try: print(f"EDID: |{record.editor_id}|")
            except: input("EDID: ERROR")
            try: print(f"NAME: |{record.full_name}|")
            except: input("NAME: ERROR")
            print("")
        

        # Keyword
        if record.type == 'KYWD':
            print("TYPE: ", record.type)
            try: print(f"FORMID: |{record.form_id.objectindex}|")
            except: input("FORMID: ERROR")
            try: print(f"EDID: |{record.editor_id}|")
            except: input("EDID: ERROR")
            try: print(f"NAME: |{record.full_name}|")
            except: input("NAME: ERROR")
            print("")
        

        # TODO DeathItem


        # Faction
        if record.type == 'FACT':
            print("TYPE: ", record.type)
            try: print(f"FORMID: |{record.form_id.objectindex}|")
            except: input("FORMID: ERROR")
            try: print(f"EDID: |{record.editor_id}|")
            except: input("EDID: ERROR")
            try: print(f"NAME: |{record.full_name}|")
            except: input("NAME: ERROR")
            print("")
        

        # TODO SleepOutfit
        # TODO Skin


        # # DEBUG
        # continue


        # Pass the following (NPC section) if not a NPC
        if record.type != 'NPC_': continue
        # TODO Add Leveled NPC ('LVLN')? --> seem to work without this, by using "classic path" (further tests needed)

        print("TYPE: ", record.type)
        try: print(f"FORMID: |{record.form_id.objectindex}|")
        except: print("FORMID: ERROR")

        data = record.content

        i = data.find(b'FULL')
        j = data.find(b'EDID')

        # # DEBUG - Pass NPC if no Name or no EditorID
        # if i == -1 or j == -1: continue

        full = None if i == -1 else b''.join(data[i:].split(b'\x00')[1:2]).decode('utf-8')
        edid = None if j == -1 else b''.join(data[j:].split(b'\x00')[1:2]).decode('utf-8')
        # NOTE More risky to do: data[i:].split(b'\x00')[1] and data[j:].split(b'\x00')[1]
        # NOTE Alternatively, use i+4 (or j+4) as slicing starting index since len('FULL') and len('EDID')... are 4 chars

        # if not full: continue

        print(f"EDID: |{edid}|")
        print(f"NAME: |{full}|")
        print("")
