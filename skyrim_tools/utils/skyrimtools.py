from pathlib import Path
import logging

# from constants import MO2_ROOT
# from lib import Condition, Mod
# from utils.file_io import get_lines, strip_words, load_json
# import utils.file_io as fio
# import utils.dataformat as df
from . import file_io as fio
from . import dataformat as df



# DECLINAISONS
def get_spid_files(folder: Path) -> list[Path]:     # Spell Perk Item Distributor
    return fio.find_matching_files(folder=folder, pattern="_DISTR", pos="r", suffix="ini")

def get_kid_files(folder: Path) -> list[Path]:      # Keyword Item Distributor
    return fio.find_matching_files(folder=folder, pattern="_KID", pos="r", suffix="ini")

def get_cid_files(folder: Path) -> list[Path]:      # Container Item Distributor
    return fio.find_matching_files(folder=folder, pattern="_CID", pos="r", suffix="ini")

def get_bos_files(folder: Path) -> list[Path]:      # Base Object Swapper
    return fio.find_matching_files(folder=folder, pattern="_SWAP", pos="r", suffix="ini")

def get_aos_files(folder: Path) -> list[Path]:      # AnimObject Swapper
    return fio.find_matching_files(folder=folder, pattern="_ANIO", pos="r", suffix="ini")

def get_flm_files(folder: Path) -> list[Path]:      # FormList Manipulator
    return fio.find_matching_files(folder=folder, pattern="_FLM", pos="r", suffix="ini")

def get_plugins(folder: Path, _sorted: bool = False, _fast_parser: bool = True, _alpha_order: bool = False) -> "list[Path]|dict":
    SUFFIXES = ("esm", "esp", "esl")

    if _sorted:
        modlist = { s: fio.find_matching_files(folder=folder, suffix=s, child_only=_fast_parser) for s in SUFFIXES }
        if _alpha_order: modlist = { suffix: sorted(plugins, key=lambda x: x.name.lower()) for suffix, plugins in modlist.items() }
    else:
        modlist = [f for s in SUFFIXES for f in fio.find_matching_files(folder=folder, suffix=s, child_only=_fast_parser)]
        if _alpha_order: modlist = sorted(modlist, key=lambda x: x.name.lower())
    
    return modlist

def get_mods(files: "list[Path]|Path") -> list[Path]:           # TODO (mod folders in MO2)
    return []



def get_ini_contents(files: "list[Path]|Path", merge: bool = False) -> "str|dict":
    contents = "" if merge else {}

    if isinstance(files, Path): files = [files]

    for f in files:
        file_content = fio.load_data(f)
        if not isinstance(file_content, str): continue
        
        if isinstance(contents, str):
            contents += f"\n{file_content}"
        elif isinstance(contents, dict):
            contents[str(f)] = file_content
    
    return contents
