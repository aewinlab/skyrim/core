"""Description of the package here
"""                                     # TODO add description
#                                       # TODO Add reference to elder-scrolls (https://github.com/sinan-ozel/elder-scrolls/tree/main)
#                                       #                       & tes-reader (https://github.com/sinan-ozel/tes-reader/tree/main)
#                                       #                       by Sinan Ozel (https://github.com/sinan-ozel)
__version__ = '0.2.4'
__author__ = 'Je... Co...'              # 'DocHash (aka Aewin or DaimyoTaiShi)'



from pathlib import Path

from .constants import FRAMEWORK_MAP
from .utils import file_io as fio
from .lib.plugin import Mod
from .lib.models import RecordRef



# # TODO To uncomment
# def get_plugins(folder: Path, sorted: bool = False) -> "list[Path]|dict":
#     SUFFIXES = ("esm", "esp", "esl")

#     if sorted:
#         modlist = { s: fio.find_matching_files(folder=folder, suffix=s) for s in SUFFIXES }
#     else:
#         modlist = [f for s in SUFFIXES for f in fio.find_matching_files(folder=folder, suffix=s)]
    
#     return modlist


def get_references(files: "list[Path]|Path", framework: "str|None" = None) -> dict:
    references = {}
    parser_options = { 'framework_model': FRAMEWORK_MAP.get(framework.lower()) if framework else None }

    if isinstance(files, Path): files = [files]

    for f in files:
        file_records = Mod(path=f, _options=parser_options).records
    
        # Add the new references found in current plugin to the references
        references = {
            k: references.get(k, []) + file_records.get(k, [])
            for k in set(references | file_records)
            }
        
    # Sort references alphabetically
    references = {
        record_type: sorted(records, key=lambda x: x.editor_id.lower())
        for record_type, records in references.items()
        }
    
    return references


# # TODO To uncomment
# def get_ini_contents(files: "list[Path]|Path", merge: bool = False) -> "str|dict":
#     contents = "" if merge else {}

#     if isinstance(files, Path): files = [files]

#     for f in files:
#         file_content = fio.load_data(f)
#         if not isinstance(file_content, str): continue
        
#         if isinstance(contents, str):
#             contents += f"\n{file_content}"
#         elif isinstance(contents, dict):
#             contents[str(f)] = file_content
    
#     return contents
