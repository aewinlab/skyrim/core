# from .constants import MO2_ROOT
from constants import MODS_FOLDER, TMODS, SPID_TYPES

from lib.plugin import ElderScrollsFile, Mod
from lib.record.id import FormId
import utils.skyrimtools as skyrim
import utils.file_io as fio
import utils.dataformat as df


from pprint import pprint
from pathlib import Path
import os
# # from tes_reader import Reader
# # from tes_reader.record_types import NPC
# from elder_scrolls.lib import Loader
# from elder_scrolls.elder_scrolls_file import ElderScrollsFile
# from elder_scrolls.record import NPC_
# from elder_scrolls.form_id import FormId




# exec(open('skyrim_tools/main.py').read())
if __name__ == "__main__":
    
    ##### GET plugin file(s) content
    mod_path = MODS_FOLDER / "/".join(TMODS.get('lotd', 'dun_merch'))
    # NOTE Possible key values: 'falskaar', 'lotd', 'lotd_obis', 'lotd_ccor', 'lotd_waccf', 'dun_merch'

    # type_list = [value for value in SPID_TYPES.values() if value is not None]
    type_list = [v if isinstance(v, str) else i for v in SPID_TYPES.values() for i in (v if isinstance(v, list) else [v]) if v is not None]
    print(type_list)

    plugin = Mod(path=mod_path)
    # pprint(vars(plugin))
    pprint(plugin.records)

    exit()







    #region TO REMOVE
    mod_content = {}
    # with Loader(file_path=mod_test_path) as plugin:
    with ElderScrollsFile(file_path=mod_test_path) as plugin:
        print(vars(plugin))
        print(plugin.record_count)
        mod_content = plugin

        # print(list(plugin._file))


        print(vars(plugin.header_record))
        print(plugin.header_record.content)

        print(list(plugin.header_record.get_all_fields()))
        # print(list(plugin.header_record))

        for r in plugin._get_all_records():
            print(vars(r))
        
        records = [r for r in plugin._get_all_records()]
        for r in records:
            if r.type == "LVLI":
                # print(r.content)
                print(r)
                form = FormId('0x0003ffff')
                form2 = FormId('0x000d9f89')
                print(form._bytes)
                print(form2._bytes)
                print(r._header)
                print(r.form_id)
                print(r.editor_id)
                print(r.type)

            if r.type == "BOOK":
                print(list(r.get_all_fields()))
                print("***********")
                print(r.content)
                print(r.__repr__())
                # print(list(plugin._get_record_at_position(6638)))
                
                # print(vars(r))
                # print(r.get_field('FORM'))
                # print(r.get_field('KSIZ'))

                
                print(r.size)
                for i in r.get_all_fields():
                    print(i)
                print("----------------------")
            # if r.full_name != "None":
            #     print(r.full_name)                      # Nom des livres

            # # FormID
            # form_id = FormId('0x0001afd2')
            # form_id = FormId(b'\xd2\xaf\x01\x00')
            # print(form_id)
            # print(form_id.modindex)                     # int
            # print(form_id.objectindex)                  # hex
            # print(form_id._bytes)                       # bytes
    print("======================================")
    print(mod_content)
    records = [r for r in mod_content._get_all_records()]
    for record in records:
        print(record)
    exit()
    #endregion

    ##### GET plugin file(s) content




    ##### GET SPID file(s) content
    # files = get_mods(MO2_ROOT, sorted=True)
    files = skyrim.get_spid_files(MO2_ROOT)
    # files = find_matching_files(MO2_ROOT, "_DISTR", "r", "ini")
    print(files)
    for f in files:
        # files_content = fio.load_data(files[0])
        files_content = fio.load_data(f)
        if isinstance(files_content, str):
            files_content = files_content.splitlines()
            files_content = df.clean_strings(files_content)
            for i in files_content: print(i)
    # print(files_content)
    exit()