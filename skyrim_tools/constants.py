from pathlib import Path



MO2_ROOT = Path(__file__).parent.parent / "mo2"



SPID_TYPES = {
    'spell': 'SPEL',                # Spell
    'perk': 'PERK',                 # Perk
    'item': [                       # Item
        'SCRL',
        'ARMO',
        'BOOK',
        'INGR',
        'MISC',
        'WEAP',
        'AMMO',
        'KEYM',
        'ALCH',
        'SLGM',
        'LVLI'
    ],
    'shout': 'SHOU',                # Shout
    'levspell': 'LVSP',             # LevSpell
    'package': 'PACK',              # Package
    'outfit': 'OTFT',               # Outfit
    'keyword': 'KYWD',              # Keyword
    'deathitem': None,              # DeathItem
    'faction': 'FACT',              # Faction
    'sleepoutfit': None,            # SleepOutfit
    'skin': None,                   # Skin
    'actor': 'NPC_'
}

CID_TYPES = {
    'item': [                       # Item
        'SCRL',
        'ARMO',
        'BOOK',
        'INGR',
        'MISC',
        'WEAP',
        'AMMO',
        'KEYM',
        'ALCH',
        'SLGM',
        'LVLI'
    ],
    'container': 'CONT'             # Container
}

FRAMEWORK_MAP = {
        "spid": SPID_TYPES,
        "cid": CID_TYPES
}



#region Tests
MODS_FOLDER = Path('D:/Mods/MO2-SSE/mods/')                                                 # MO2 mods root

TMODS = {                                                                                   # Mods to test features
    "lotd": ("LoTD - Legacy of the Dragonborn", "LegacyoftheDragonborn.esm"),
    "lotd_obis":  ("TEST LoTD patches TEMP A REINSTALL", "DBM_OBIS_Patch.esp"),
    "lotd_ccor":  ("TEST LoTD patches TEMP A REINSTALL", "DBM_CCOR_Patch.esp"),
    "lotd_waccf":  ("TEST LoTD patches TEMP A REINSTALL", "DBM_WACCF_Patch.esp"),
    "falskaar": ("Falskaar", "Falskaar.esm"),
    "dun_merch":  ("colporteurs_de_morrowind_", "mihaildunmermerchants.esp")
}
#endregion
