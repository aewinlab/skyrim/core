from pathlib import Path
from setuptools import setup, find_packages
from skyrim_tools import __version__ as version

# HERE = Path(__file__).parent
# README = (HERE / "README.md").read_text()                 # README.md to edit

setup(
    name="skyrim-tools",
    version=version,
    # description="TODO Description.",
    # long_description=README,
    # long_description_content_type="text/markdown",
    # url="https://gitlab.com/aewinlab/skyrim/core",
    # author="Je... Co...",                                 # "DocHash (aka Aewin or DaimyoTaiShi)"
    # license="Creative Commons Zero v1.0 Universal",       # TODO set value & add LICENSE file
    packages=find_packages()                                # ['core'] ; put setup.py out of package?
)


# Then create the package using bash command `python setup.py sdist` into root dir
# A tar.gz or zip file will be created containing the package
# To install the package, use bash command `pip install PACKAGE_NAME.tar.gz`
